# Multiscale FEM for CFD 

Simulating Computational Fluid Dynamics (CFD) with Finite Element Methods (FEM) can be hard. Thus specialized methods are required. 

Typically one the CFD involves multi-scale phenomena such as turbulence. To resolve it properly one needs a very fine resolution of the mesh which however causes the computational complexity to increase. One approach to avoid this is to first simulate on a course scale and then use refinement techniques to scale the solution to finer meshes or higher order elements.

<br>

Here I present two such refinement techniques.

<br>
<br>

## Fine-Scale Green's Operator

The simulation of turbulent CFD requires solving Partial Differential Equations (PDEs) at very fine resolutions. The solution of the PDE is called Green's function. The coarse-scale Green's function is computed using standard FEM while the fine-scale solution is treated as a correction term obtained by applying the fine-scale Green’s operator to the residual of the coarse solution. This correction, known as stabilization, refines the solution.

As an example this multiscale refinement method is applied to an Advection-Diffusion PDE model, where coarse scales represent overall behavior, and fine scales capture localized turbulence. Advection-dominated problems, being more turbulent than diffusion-dominated ones, particularly benefit from stabilization techniques.

- [Text Version](https://gitlab.com/luziffer/fem_refinement_for_cfd/-/raw/main/FineScaleGreenOperator/finescalegreen_2020_lucalenz.pdf?inline=false)

- [Presentation Slides](https://gitlab.com/luziffer/fem_refinement_for_cfd/-/raw/main/FineScaleGreenOperator/presentation_finescalegreen_2020_lucalenz.pdf?inline=false)


<br>
<p align="center">
    <img src="https://gitlab.com/luziffer/fem_refinement_for_cfd/-/raw/main/FineScaleGreenOperator/image.png?inline=false" width=300px>
</p>
<br>

## Discontinuous Galerkin Methods 

The solutions to the PDEs are typically continuously differenciable in space or even smooth. However, higher order FEM elements typically require complex refinement strategies while the zero-order elements are discontinuous. 

Penalty terms for discontinuous variational FEM methods are derived. Analytical theory is provided and consistency is proven. 

Here as a concrete example we consider the Galerkin method on incompressible Pressure-Diffusion (Stokes) equations and derive the Symmetric Interior Penalty (SIP) method. 


- [Presentation Slides](https://gitlab.com/luziffer/fem_refinement_for_cfd/-/raw/main/DiscontinuousGalerkin/presentation_dgmstokes_lucalenz_2021.pdf?inline=false)

<br>
<p align="center">
    <img src="https://gitlab.com/luziffer/fem_refinement_for_cfd/-/raw/main/DiscontinuousGalerkin/image.png?inline=false" width=300px>
</p>
<br>

